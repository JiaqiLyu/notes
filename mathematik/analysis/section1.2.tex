\section{Die natürlichen Zahlen und das Induktionsprinzip}

Wenn wir die natürlichen Zahlen in der üblichen Form

\[
  \mathbb{N} = \{1,2,3,\dots\}
\]

anschreiben, bedeuten die $\dots$, dass es einen wohldefinierten Zählvorgang gibt, der jeder natürlichen Zahl in eindeutiger Weise einen Nachfolger zuordnet.  Dies ist die wesentliche charakterisierende Eigenschaft der natürlichen Zahlen. Mit Hilfe der Grundbegriffe ``Menge'' und ``Abbildung'' können wir die natürlichen Zahlen in folgender Weise vollständig charakterisieren:

Axiomatische Charakterisierung der natürlichen Zahlen (Dedekind, 1888; Peano, 1889):

Die natürlichen Zahlen bilden eine Menge $\mathbb{N}$ mit den folgenden Eigenschaften:

\begin{itemize}
  \item[(P1)] Es gibt ein Element $1 \in \mathbb{N}$
  \item[(P2)] Es gibt eine Abbildung $\nu: \mathbb{N} \rightarrow \mathbb{N}$, sodass
  \item[(P3)] $1 \notin \nu(\mathbb{N})$,
  \item[(P4)] $\nu$ ist injektiv,
  \item[(P5)] ist $M\subset\mathbb{N}$ mit $1 \in M$ und $\nu(M) \subset M$, so gilt bereits $M=\mathbb{N}$.
\end{itemize}

(``Peano-Axiome'' der natürlichen Zahlen)

\vspace{\baselineskip}

Die Abbildung $\nu$ wird als Nachfolgeabbildung bezeichnet. Dieser beginnt mit 1 (P3), und (P4) sagt aus, dass keine Zahl dabei mehr als einmal erreicht wird. Schließlich umfasst das Axiom (P5) die Aussage, dass jede natürliche Zahl durch den Zählvorgang erfasst wird.

Durch die Axiome (P1) bis (P5) sind die natürlichen Zahlen im wesentlichen (d.h. ``bis auf Isomorphie'') eindeutig festgelegt. Genauer gilt der folgende

\paragraph{Eindeutigkeitssatz (Dedekind, 1888)} Ist $\mathbb{N}'$ eine Menge mit einem ausgezeichneten Elemen $1'\in\mathbb{N}'$ und einer Abbildung $\nu':\mathbb{N}'\rightarrow\mathbb{N}'$, die die Axiome (P3) bis (P5) erfüllt, so existiert eine Abbildung $\phi: \mathbb{N} \rightarrow \mathbb{N}'$ mit $\phi(1)=1'$ und $\phi \circ \nu = \nu' \circ \phi$. (Ebbinghaus u.a., Zahlen, Kap. 1, $\S$2)

(Die Abbildung $\phi$ wird als Isomorphismus bezeichnet, das bedeutet ``strukturerhaltende Abbildung''.)

Aufgrund dieses Eindeutigkeitssatzes können wir von axiomatischer Charakterisierung der natürlichen Zahlen sprechen. In ähnlicher Weise sollen in den nächsten Abschnitten die reellen Zahlen eingeführt werden.

Die Peano-Axiome der natürlichen Zahlen, insbesondere das Axiom (P5), sind noch aus einem zweiten Grund für die Analysis relevant. Sie liefern nämlich ein äußerst nützliches Beweisverfahren, den Induktionsbeweis.

Um dies zu erläutern, schreiben wir $\nu(n) = n + 1$. Dann lautet das sogenannte Induktionsaxiom (P5) folgendermaßen:

\begin{itemize}
  \item[(P5')] Ist $M\subset \mathbb{N}$ eine Menge mit $1 \in M$ und der Eigenschaft $n \in M \Rightarrow n + 1 \in M$, so gilt $M = \mathbb{N}$.
\end{itemize}

Sei nun $A(n)$ eine Aussage, die von einem Parameter $n \in \mathbb{N}$ (ggf. auch $n \in \mathbb{Z}$) abhängt, z.B:

\paragraph{$A(n)$} Die Summer der ersten n natürlichen Zahlen beträgt $\frac{1}{2}n(n+1)$, in Zeichen: $\sum\limits_{k=1}^{n}k=\frac{1}{2}n(n+1)$, wobei $\sum\limits_{k=1}^{n}k=1 + 2 + \dots + k$ ist.

\vspace{1em}
oder

\paragraph{$B(n)$} Jede natürliche Zahl n lässt sich als Produkt von Primzahlen darstellen. (Existenz einer ``Primfaktorzerlegung'')

Um derartige Aussagen zu beweisen, können wir auf die nachstehende Folgerung aus dem Induktionsaxiom (P5') zurückgreifen:

\begin{theorem}{(Induktionsprinzip, 1. Version)}
  Es sei $n_{0} \in \mathbb{Z}$ und für $n\in \mathbb{Z}$ mit $n \geq n_{0}$ sei eine Aussage $A(n)$ gegeben. Hierfür gelten:

  \begin{enumerate}
    \item $A(n_{0})$ ist richtig,
    \item $\forall n \geq n_{0} : A(n) \Rightarrow A(n+1)$.
  \end{enumerate}

  Dann gilt $A(n)$ für alle $n \geq n_{0}$.
\end{theorem}

\begin{proof}
  \renewcommand{\qedsymbol}{}
  Wende (P5') an auf $M=\{n \in \mathbb{N} : A(n + n_{0} - 1) \text{ ist richtig}\}$.

  Die Anwendung dieses Prinzips soll an zwei Beispielen erläutert werden:
\end{proof}

\paragraph{Bsp. 1:} $\sum\limits_{k=1}^{n}k=\frac{1}{2}n(n+1)$ \hfill ($A(n)$)

\begin{proof}
  \renewcommand{\qedsymbol}{}
  \begin{enumerate}
    \item[($i$)] Für $n=1$ haben wir einerseits $\sum\limits_{k-1}^{1}k=1$, andererseits $\frac{1}{2}*1(1+1)=\frac{1}{2}*2=1$. Also stimmt die Aussage für $n=1$. (``Induktionsanfang'')
    \item[($ii$)] Wir nehmen die Gültigkeit von $A(n)$ an, hier $\sum\limits_{k=1}^{n}k=\frac{1}{2}n(n+1)$ (Das ist die sog. Induktionsvorraussetzung). Daraus folgen wir die Gültigkeit von $A(n+1)$, im vorliegenden Beispiel geschieht das folgendermaßen:

      \begin{align*}
        \sum_{k=1}^{n}k=\frac{1}{2}n(n+1) &\Rightarrow \sum_{k+1}^{n+1}k\\
                                          &= \sum_{k=1}^{n}k + n+1\\
                                          &= \frac{1}{2}n(n+1) + n+1\\
                                          &= \frac{1}{2}(n+1)(n+2)
      \end{align*}

      und damit ist $A(n+1)$ gezeigt. Dieser zweite Schritt wird als Induktionsschluß oder auch als Induktionsschritt bezeichnet.
  \end{enumerate}
\end{proof}

\paragraph{Bsp. 2:} Anzahl der $k$-elementigen Teilmengen einer $n$-elementigen Menge

\begin{proof}
  \renewcommand{\qedsymbol}{}
  Um hier eine Aussage $A(n)$ formulieren zu können, benötigen wir zunächst einige Bezeichnungen:
\end{proof}

\begin{definition}
  Für $n\in\mathbb{N}$ definieren wir die \emph{Fakultät} durch $n! := \prod\limits_{k=1}^{n}k := 1 * 2 * \dots * k$.

  Ferner setzen wir $0! := 1$.

  (Bsp. $1!=1, 2!=1*2=2, 3!=1*2*3=6, 4!=1*2*3*4=24, \dots$)
\end{definition}

\begin{definition}
  Für $n\in\mathbb{N}_{0}$ und $0 \leq k \leq n$ heißt die Zahl

  \[
    \binom{n}{k} := \frac{n!}{k! * (n-k)!}
  \]

  der \emph{Binomialkoeffizient}. Ferner wird die Konvention $\binom{n}{k}=0$ für $k<0$ und $k>n$ vereinbart.
\end{definition}

\begin{remark}
  Unmittelbar aus der Definition folgt die Symmetrieeigenschaft $\binom{n}{k}=\binom{n}{n-k}$.
\end{remark}

\begin{lemma}\label{lemma1}
  Für $n\in \mathbb{N}_{0}$ und $k \in \{ 0, \dots, n+1\}$ gilt

  \[
    \binom{n+1}{k} = \binom{n}{k} + \binom{n}{k-1}
  \]
\end{lemma}

\begin{proof}
  Klar für $k=0$ und für $k=n+1$. Für $k \in \{1, \dots n\}$ haben wir

  \begin{align*}
    \binom{n}{k} + \binom{n}{k-1} &= \frac{n!}{k!(n-k)!} + \frac{n!}{(k-1)!(n-k+1)!}\\
                                  &= \frac{n! * (n-k+1)}{k!(n-k)!*(n-k+1)} + \frac{k*n!}{k*(k-1)!(n-k+1)!}\\
                                  &= \frac{n!(n-k+1)}{k!(n-k+1)!} + \frac{n!k}{k!(n-k+1)!}\\
                                  &= \frac{n!(n-k+1)+n!k}{k!(n-k+1)!}\\
                                  &= \frac{n!(n+1-k+k)}{k!(n-k+1)!}\\
                                  &= \frac{(n+1)!}{k!(n+1-k)!}\\
                                  &=\binom{n+1}{k}
  \end{align*}
\end{proof}

Kommen wir zurück zur Anzahl der Teilmengen:

Behauptung: Die Anzahl der $k$-elementigen Teilmengen einer $n$-elementigen Menge beträgt $\binom{n}{k}$.

(Hierbei: $n\in\mathbb{N}_{0}$, $0\leq k \leq n$)

\begin{proof}
  Per Induktion über $n$, beginnend mit $n=0$:

  Induktionsanfang: $n=0$. Die leere Menge hat nur sich selbst als Teilmenge, sie hat null Elemente. Also: Die Anzahl der nullelementigen Teilmengen einer nullelementigen Menge beträgt $1=\binom{0}{0}$.

  Induktionsschluss: $n \rightarrow n+1$. Sei $M_{n+1}=\{a_{1}, \dots, a_{n+1}\}$ und $M_{n}=\{a_{1}, \dots, a_{n}\}$. Für $k=0$ kommt wieder nur die leere Menge als null($=k$)-elementige Teilmenge von $M_{n+1}$ in Frage uns es gilt $\binom{n+1}{0}=1$. Betrachte also $k \in \{1, \dots, n+1\}$:

  Hierfür haben wir

  \begin{align*}
    &\# \{M \subset M_{n+1} : \#M = k\}\\
    &= \#\{M \subset M_{n} : \#M = k\} + \#\{M \subset M_{n+1} : \#M = k \text{ und } a_{n+1} \in M\}\\
    &= \binom{n}{k} + \#\{M' \subset M_{n} : \#M' = k-1\}\\
    &= \binom{n}{k} + \binom{n}{k-1}
  \end{align*}

  Jetzt folgt die Behauptung aus \autoref{lemma1}.
\end{proof}

Folgerung: $\binom{n}{k} \in \mathbb{N}_{0}$.

Die erste Version des Induktionsprinzips erlaubt es noch nicht (ohne weiteres), die Aussage über die Existenz einer Primfaktorzerlegung zu beweisen. Die Aussage für den direkten Vorgäger, den die Induktionsvoraussetzung liefert, ist hierfür allein nicht ausreichend. Wir beweisen die folgende allgemeinere Variante:

\begin{theorem}{(Induktionsprinzip, 2. Version)}
  Ist $M \subset \mathbb{N}$ eine Teilmenge mit

  \begin{enumerate}
    \item[($i$)] $1 \in M$,
    \item[($ii$)] $\{1, \dots, n\} \subset M \Rightarrow n+1 \in M$.
  \end{enumerate}

  Dann ist bereits $M=\mathbb{N}$.
\end{theorem}

\begin{proof}
  $\widetilde{M} := \{n \in \mathbb{N} : \{1, \dots, n\} \subset M\}$. Dann ist $1 \in \widetilde{M}$ und $n \in \widetilde{M} \Rightarrow \{1, \dots, n\} \subset M \Rightarrow n+1 \in M$, also $\{1, \dots, n+1\}\subset M\Rightarrow n+1 \in \widetilde{M}$. Nach der 1. Variante des Induktionsprinzips ist also $\widetilde{M}=\mathbb{N}$. Damit $\{1, \dots, n\} \subset M$ für alle $n\in \mathbb{N}$. Folglich $M=\mathbb{N}$.
\end{proof}

Hieraus ergibt sich:

Folgerung: Ist $A(n)$ eine Aussage, sodass gilt

\begin{enumerate}
  \item[($i$)] Es existiert $n_{0}\in \mathbb{Z}$ mit $A(n_{0})$ ist richtig,
  \item[($ii$)] $A(n_{0}), \dots, A(n) \Rightarrow A(n+1) \forall n \geq n_{0}$.
\end{enumerate}

Dann ist $A(n)$ richtig für alle $n \geq n_{0}$.

(Wende die ``2. Version'' an auf $M=\{n \in \mathbb{N} : A(n + n_{0} - 1)\}$.)

Damit ist die Gültigkeit von $B(2)$ leicht zu zeigen:

\paragraph{Forts. Bsp:}

$B(2)$ ist richtig: $2=2$ ist eine Primfaktorzerlegung. (Dies ist der Induktionsanfang)

Induktionsschluss: Ist $n+1$ prim, so ist $n+1=n+1$ eine Primfaktorzerlegung. Andernfalls existieren $p = p_{1} \cdots p_{l}$ und $q = q_{1} \cdots q_{k}$ mit Primzahlen $p_{1}, \dots, p_{l}, q_{1}, \dots, q_{k}$, sodass $n+1 = p * q = p_{1} * \dots * p_{l} * q_{1} * \dots * q_{k}$, und damit ist eine Primfaktorzerlegung von $n+1$ gefunden. (Hier geht die Induktionsvoraussetzung für beliebige $p,q\in\mathbb{N}$ mit $p,q\leq n$ ein!)

\begin{remark}{Abschließende Bemerkung zur Induktion}
  Häufig werden sogenannte Folgen von Zahlen (oder auch anderen Objekten) rekursiv definiert durch

  \[
    x_{0} = a_{0}, x_{1} = a_{1}, \dots, x_{m} = a_{m} \text{ (die $a_{i}$ werden fest vorgegeben) }
  \]

  und eine Vorschrift $x_{n+1} = f(x_{n}, \dots, x_{n-m})$, wobei $n \geq m$. Dass dadurch tatsächlich $x_{n}$ für alle natürlichen Zahlen $n$ definiert ist, beruht auf dem Induktionsprinzip. Bsp: $x_{0}=1, x_{n+1}=(n+1)*x_{n}$ liefert die Fakultät, $x_{0} = 0, x_{1} = 1, x_{n+1} = x_{n} + x_{n-1}$ die sogenannten Fibonacci-Zahlen.
\end{remark}
