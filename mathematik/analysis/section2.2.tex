\section{Anordnungsaxiome und Archimedesches Axiom}

\begin{definition}
    Ein Körper $(K,+,\cdot)$ heißt \emph{angeordnet}, falls eine Teilmenge $K^+ \subset K$ existiert mit
    \begin{enumerate}[label=(A\arabic*)]
        \item Für alle $x \in K$ gilt genau eine der Beziehungen $x \in K^+$, $-x \in K^+$ oder $x=0$,
        \item $x,y \in K^+ \Rightarrow x+y \in K^+$,
        \item $x,y \in K^+ \Rightarrow xy \in K^+$.
    \end{enumerate}
\end{definition}

\begin{remark}
    $K^+$ heißt die Menge der positiven Zahlen. (A2) und (A3) sagen aus, dass $K^+$ abgeschlossen ist unter $+$ und $\cdot$.
\end{remark}

Durch die Existenz der Menge $K^+$ ist in natürlicher Weise eine Anordnung auf $K$ gegeben:

\begin{definition}{($<$- und $\leq$- Beziehung)}
    \[ x<y :\Leftrightarrow y>x :\Leftrightarrow y-x \in K^+ \]
    \[ x \leq y :\Leftrightarrow y \geq x :\Leftrightarrow x<y \text{ oder } x=y \]
\end{definition}

Wir verwenden auch die folgenden

\begin{bez}
    \[ K_0^+ := \{ x \in K: x \geq 0 \} = K^+ \cup \{0\} \]
    \[ K_0^- := \{ x \in K: x<0 \} \]
\end{bez}

\begin{lemma}\label{lemma1}
    Es sei $(K,+,\cdot)$ ein angeorgneter Körper. Dann gelten für $x,y,z,a,b \in K$:
    \begin{enumerate}
        \item $x<y \Leftrightarrow x+z<y+z$,
        \item $x<y \text{ und } a<b \Rightarrow x+a<y+b$,
        \item $x<y \text{ und } y<z \Rightarrow x<z$ (Transitivität),    %im Skript x<y, evtl. ein Schreibfehler
        \item $x<y \text{ und } 0<a \Rightarrow ax<ay$,
        \item $x<y \text{ und } a<0 \Rightarrow ax>ay$,
        \item $0 \leq x<y \text{ und } 0 \leq a<b \Rightarrow ax<by$,
        \item $x \neq 0 \Rightarrow x^2>0$ (insbes. $1>0$),
        \item $x>0 \Rightarrow \frac{1}{x} >0$,
        \item $0<x<y \Rightarrow \frac{1}{y} < \frac{1}{x}$.
    \end{enumerate}
\end{lemma}

\begin{proof}
    \begin{enumerate}
        \item $x<y \underset{\text{p. d.}}{\Leftrightarrow} y-x \in K^+ \Leftrightarrow (y+z)-(x+z) \in K^+ \underset{\text{p. d.}}{\Leftrightarrow} x+z<y+z$.
        \item $x<y$ und $a<b \underset{\text{p. d.}}{\Leftrightarrow} y-x \in K^+$ und $b-a \in K^+ \Rightarrow (y-x)+(b-a) \in K^+ \Leftrightarrow (y-b)-(x-a) \in K^+ \underset{\text{p. d.}}{\Leftrightarrow} x+a<y+b$.
        \item Setze $a=y, b=z$ in 2. Dann haben wir $x+y<y+z \underset{1.}{\Rightarrow} x<z$.
        \item N. V. ist $a>0$ und $y-x \overset{\in K^+}{>0} \underset{\text{(A3)}}{\Rightarrow} a(y-x) \overset{\in K^+}{>0} \underset{\text{(K9)}}{\Rightarrow} ay-ax \overset{\in K^+}{>0} \underset{\text{(p. d.)}}{\Rightarrow} ax<ay$.
        \setcounter{enumi}{5}
        \item Wir haben $y \in K^+$ und $b \in K^+ \underset{\text{(A3)}}{\Rightarrow} yb \in K^+$. Im Fall $a=0$ oder $x=0$ folgt die Beh. Sind $x \in K^+$ und $a \in K^+$, gilt nach 4. $ax<ay$ und $ay<by$, nach 3. also $ax<by$.%"Beh": ich kann das Wort leider nicht erkennen
        \item $x \neq 0 \underset{\text{A1}}{\Rightarrow} x>0$ oder $-x>0$.\\Im ersten Fall folgt aus 5. $x^2=x \cdot x>0$ (Setze in 6. $a=x=0,b=y=x$), im zweiten $x^2=(-x)(-x)>0$.
    \end{enumerate}
    5.,8.,9. in den Übungen.
\end{proof}

\begin{remark}
    \begin{enumerate}[label=(\roman*)]
        \item Der Körper $(\mathbb{Q},+,\cdot)$ mit der üblichen ``$<$''-Relation ist angeordnet.
        \item Die reelen Zahlen fassen wir auf als einen Körper, der den Anordnungsaxiomen (A1) -- (A3) genügt. (Wegen (i) ist dies für eine Charakterisierungnoch nicht ausreichend.)
        \item Es ist immer unmöglich, den Körper $(\mathbb{C},+,\cdot)$ so anzuordnen, dass (A1) -- (A3) erfüllt sind. Denn es ist $i^2=-1<0$, ein Widerspruch zu Eigenschaft 7.
    \end{enumerate}
\end{remark}

\begin{lemma}{Bernoullische Ungleichung}\label{lemma2}\\
    Es sei $K$ ein angeordneter Körper und $x \in K$ mit $x \geq -1$. Dann gilt für jede natürliche Zahl $n$:
    \[ (1+x)^n \geq 1+nx \text{.} \]
\end{lemma}

\begin{proof}
    Induktion über $n$, für $n=1$ gilt ``$=$''.
    \begin{align*}
        \begin{autobreak}
            n \rightarrow n+1 \text{: } (1+x)^{n+1}
            =\underbrace{(1+x)}_{\geq 0} (1+x)^n
            \underset{\text{6. und I. V.}}{\geq} (1+x)(1+nx)
            =1+(n+1)x+ \underbrace{nx^2}_{\geq 0\text{, nach 7. und (A2)}}
            \underset{\text{2.}}{\geq} 1+(n+1)x
        \end{autobreak}
    \end{align*}
\end{proof}

\begin{remark}
    Gilt mit $=$ auch für $n=0$.
\end{remark}

In einem angeordneten Körper können wir definieren, was einseitig beschränkte und beschränkte Mengen sind.

\begin{definition}
    Es sei $K$ ein angeordneter Körper und $\emptyset \neq A \subset K$.
    \begin{enumerate}
        \item $A$ heißt \emph{nach oben beschränkt}, falls ein $S \in K ??$, sodass $x \leq S$ für alle $x \in A$. In diesem Fall heißt $S$ \emph{eine obere Schranke} von $A$.%?? ist?
        \item $A$ heißt \emph{nach unten beschränkt}, falls ein $s \in K ??$, sodass $x \geq s$ für alle $x \in A$. In diesem Fall heißt $S$ \emph{eine untere Schranke} von $A$.
        \item $A$ heißt \emph{beschränkt}, wenn $A$ nach oben und unten beschränkt ist.
    \end{enumerate}
\end{definition}

Eine Menge $A$ ist in jedem Fall nach oben beschränkt, wenn sie ein größtes Element besitzt. Ein solches nennen wir das Maximum von $A$, Bezeichnungen: $max A$. Entsprechend ist $min A$ das kleinste Element von $A$, also das Minimum von $A$. Solche Elemente existieren in der Regel \emph{nicht}. Beispiel: $I=(0,1)$ besitzt weder ein Maximum noch ein Minimum. Was das Intervall $(0,1)$ hingegen aufweisen kann, sind
\begin{itemize}
    \item eine kleinste obere Schranke (nämlich $1$) und
    \item eine größte untere Schranke (nämlich $0$).
\end{itemize}

\begin{definition}
    Es sei $K$ ein angeordneter Körper und $\emptyset \neq A \subset K$.
    \begin{enumerate}
        \item $S \in K$ heißt das \emph{Supremum} von $A$ (in Zeichen: $S= \sup A= \sup_{x \in A} x$), falls gilt
            \begin{enumerate}[label=(\roman*)]
                \item $S \geq x$ für alle $x \in A$ (d. h. $S$ ist eine obere Schranke von $A$) und
                \item ist $\tilde{S} \geq x$ für alle $x \in A$, so ist $\tilde{S} \geq S$ (d. h. $S$ ist die kleinste obere Schranke von $A$).
            \end{enumerate}
        \item $s \in K$ heißt das \emph{Infimum} von $A$ (in Zeichen: $s= \inf A= \inf_{x \in A} x$), falls $-s=\sup (-A)$, dabei $-A=\{-x: x \in A\}$.
    \end{enumerate}
\end{definition}

\begin{remark}
    \begin{enumerate}[label=(\roman*)]
        \item $\inf A$ ist die größte untere Schranke von $A$.
        \item Falls existiert, sind Supremum und Infimum eindeutig bestimmt.
        \item $\{x \in \mathbb{Q} : X^2<2\}$ besitzt in $\mathbb{Q}$ weder ein Supremum noch ein Infimum (s. u.).
        \item besitzt eine Menge $A$ ein Maximum (bzw. ein Minimum),so ist dies das Supremum (bzw. das Infimum) von $A$.
    \end{enumerate}
\end{remark}

Ist die Menge $\mathbb{N}$ der natürlichen Zahlen, betrachtet als Teilmenge der angeordneten Körper $\mathbb{Q}$ bzw. $\mathbb{R}$ beschränkt? Für die rationalen Zahlen ist leicht einzusehen, dass dies nicht der Fall ist. Hier haben wir den Satz des Archimedes:

\begin{theorem}\label{satz1}
    Zu $x,y\in R^+$ existiert $n\in \mathbb{N}$ mit $nx>y$.
\end{theorem}

\begin{proof}
    $x= \frac{p}{q}$, $y=\frac{p'}{q'}$, $p,q,p',q'\in \mathbb{N}$. Dann ist $nx>y \Leftrightarrow npq'>p'q$.\\
    Wähle $n=p'q+1$ (geht, da $p'q\in \mathbb{N}$ und also einen Nachfolger hat) $\Rightarrow npq' \geq n>p'q$.
\end{proof}

Für die reelen Zahlen können wir diese Eigenschaft aus den bisher festgelegten Axiomen nicht folgern. Wir werden sie daher als ein weiteres Axiom der reelen Zahlen postulieren:

\begin{definition}
    Eine angeordneter Körper $K$ heißt archimedisch angeordnet, wenn für ihn das archimedische Axiom
    \begin{enumerate}
        \item[(A)] $\forall x,y\in K^+ \enspace \exists n\in \mathbb{N} \text{ mit } nx>y$
    \end{enumerate}
    gilt.
\end{definition}

\begin{remark}
    Es gibt angeordnete Körper, in denen das Axiom (A) \emph{nicht} gilt. Es ist daher unabhängig von (A1) bis (A3).
\end{remark}

Um einige Folgerungen aus dem archimedischen Axiom zu ziehen, sei $K$ ein archimedisch angeordneter Körper, der $\mathbb{Q}$ enthält. (Diese Folgerungen gelten also insb. für $K=\mathbb{R}$.)

\begin{deduction}
    \begin{enumerate}
    \item Zu jedem $x\in K$ existiert genau ein $K\in \mathbb{Z}$, sodass $k\leq x<k+1$. Dieses wird mit $[x]$ (= ganzzahliger Anteil, ``Gaussklammer'') bezeichnet.
        \begin{proof}
            Klar für $x=0$. Für $x>0$ existiert nach (A) ein $n\in \mathbb{N}$ mit $n>x$. Die Menge $\{k\in \mathbb{N}: x<k\leq n\}$ ist dann nichtleer und endlich, besitzt also ein Minimum. Wir einsetzen
            \[ [x]=\min \{k\in \mathbb{N}: x<k\leq n\}-1 \text{.} \]
            Dann ist $[x]\leq x<[x]+1$  (die geforderten Eigenschaften sind also erfüllt) und $x-1<[x]\leq x$, also gibt es kein weiteres derartiges $K$. Für $x<0$ setzt man
            \[ [x]=-[-x]-1 \text{.} \]
        \end{proof}
    \item Zu jedem $\varepsilon \in K^+ \enspace \exists n\in \mathbb{N}$ mit $0<\frac{1}{n}<\varepsilon$.
        \begin{proof}
            Nach (A) $\exists n \in \mathbb{N}$ mit $n>\frac{1}{\varepsilon}$. Aufgrund der Folgerungen aus (A1) -- (A3) gilt $0<\frac{1}{n}<\varepsilon$.
        \end{proof}
    \item Ist $x\in K$ mit $x<\frac{1}{n}$ für alle $n\in \mathbb{N}$, so ist $x\leq 0$. (Klar, denn nach 2. ist $x\notin K^+$.)\\
         Natürlich zu Beweiszwecken: Um $a\leq b$ zu zeigen, reicht das Beweis der etwas shwächeren Aussage:
         \[\forall n\in \mathbb{N} \text{ ist } a\leq b+\frac{1}{n} \text{ bzw. } \forall \varepsilon \in K^+ \text{ ist } a\leq b+\varepsilon \text{.} \]
    \item Ist $q>1$, so existiert zu jedem $R>0$ ein $n\in \mathbb{N}$ mit $q^n>R$.
        \begin{proof}
            \[ q^n=(1+(q-1))^n \underset{\text{Bernoulli}}{\geq} 1+n \underbrace{(q-1)}_{>0} \]
            Nach (A) $\exists n\in \mathbb{N}$ mit $n(q-1)>R-1$. Hierfür ist dann auch $q^n>R$.
        \end{proof}
    \item Ist $0<q<1$, so existiert zu jedem $\varepsilon>0$ ein $n\in \mathbb{N}$, sodass $0<q^n<\varepsilon$.
        \begin{proof}
            Nach 4. $\exists n\in \mathbb{N}$ mit $(\frac{1}{q})^n>\frac{1}{\varepsilon}$. Hierfür gilt dann auch $q^n<\varepsilon$.
        \end{proof}
    \end{enumerate}
\end{deduction}


Zum Ende dieses Abschnitts soll noch der Absolutbetrag eingeführt und erklärt werden, was wir unter Beschränktheit einer Menge Komplexer Zahlen verstehen wollen: Zur Vorbereitung definieren wir

\begin{proof}{(Quadratwurzel)}
    Für $a\in \mathbb{R}^+$ verstehen wir unter $\sqrt{a}$ die positive Lösung $x$ der Gleichung $x^2=a$. Ferner setzen wir $\sqrt{0}:=0$.
\end{proof}

\begin{remark}
    \begin{enumerate}[label=(\roman*)]
        \item Existenz wird später gezeigt.
        \item Es gilt $a<b \Leftrightarrow \sqrt{a} < \sqrt{b}$ und $a=b \Leftrightarrow \sqrt{a} = \sqrt{b}$. Insbes. ist $\sqrt{a}$ eindeutig bestimmt.
            \begin{proof}
                Es ist $0<b-a=(\sqrt{b} - \sqrt{a}) \underbrace{(\sqrt{b} + \sqrt{a})}_{>0} \Leftrightarrow \sqrt{b} > \sqrt{a}$. (Genauso für $=$.)
            \end{proof}
    \end{enumerate}
\end{remark}

\begin{definition}{(Betrag einer komplexen Zahl)}
    Für $z=x+iy\in \mathbb{C}$ heißt $\abs{z} := \sqrt{z\cdot \bar{z}} = \sqrt{x^2+y^2}$ der \emph{Betrag} von $z$.
\end{definition}

\begin{remark}
    Für $z=x\in \mathbb{R}$ ist $\abs{z}=\abs{x}=\sqrt{x^2}=
    \begin{cases}
        x,&\text{falls} x\geq 0 \\
        -x,&\text{falls} x\leq 0
    \end{cases}$
\end{remark}

\begin{lemma}\label{lemma3}
    Für $z,w\in \mathbb{C}$ gelten: (Beachte: All diese Eigenschaften gelten auch für $z,w\in \mathbb{R}$!)
    \begin{enumerate}
        \item $\abs{z} \geq 0$ mit $\abs{z}=0 \Leftrightarrow z=0$,
        \item $\abs{z} = \abs{\bar{z}} = \abs{-z}$,
        \item $\abs{\Re z}\leq \abs{z}$, $\abs{\Im z}\leq \abs{z}$.
        \item $\abs{zw} = \abs{z} \abs{w}$ und $\abs{\frac{1}{z}} = \frac{1}{\abs{z}}$, falls $z \neq 0$
        \item $\abs{z+w}\leq \abs{z} + \abs{w}$ (Dreiecksungleichung)
        \item $\abs{\abs{z} - \abs{w}} \leq \abs{z-w}$.
    \end{enumerate}
\end{lemma}

\begin{proof}
    \begin{enumerate}
        \item $\abs{z} \geq 0$ unmittelbar aus der Def.
            \[ \abs{z} \Leftrightarrow \abs{z}^2=0 \Leftrightarrow \abs{z} \cdot \abs{\bar{z}} =0 \Leftrightarrow z=0 \lor \bar{z} =0 \Leftrightarrow z=0 \]
        \item $(-y)^2=y^2$, klar.
        \item $x^2 \leq x^2+y^2 \underset{\text{Bem. (ii) oben}}{\Rightarrow} \abs{x} \leq \abs{z}$, ebenso: $\abs{y} \leq \abs{z}$.
        \item $\abs{zw}^2=zw\bar{z}\bar{w}=z\bar{z}w\bar{w}=(\abs{z}\abs{w})^2$,
                \[ \abs*{\frac{1}{z}}^2 = \abs*{\frac{\bar{z}}{z\bar{z}}}^2 = \frac{\bar{z}}{\abs{z}^2} \cdot \frac{z}{\abs{z}^2} = \frac{1}{\abs{z}^2} \]
        \item $\abs{z+w}^2 = \abs{z}^2 + 2 \Re (z\bar{w}) + \abs{w}^2 \underset{\text{3. 4.}}{\leq} \abs{z}^2 +2\abs{z} \abs{w} + \abs{w}^2 \leq (\abs{z} + \abs{w})^2$
        \item $\abs{z} = \abs{z-w+w} \underset{\text{5.}}{\leq} \abs{z-w} + \abs{w} \Rightarrow \abs{z} - \abs{w} \leq \abs{z-w}$, ebenso: $\abs{w} - \abs{z} \leq \abs{z-w} \Rightarrow \abs{\abs{z} - \abs{w}} \leq \abs{z-w}$.
    \end{enumerate}
\end{proof}

\begin{definition}
    $A \subset \mathbb{C}$ heißt beschränkt, wenn die Menge $\{ \abs{z}: z \in A \} \subset \mathbb{R}$ beschränkt ist.
\end{definition}
