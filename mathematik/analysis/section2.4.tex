\section{Das Vollständigkeitsaxiom}

\begin{definition}
    Eine Folge $(a_n)$ komplexer Zahlen heißt eine \emph{Cauchy-Folge}, falls gilt\\
    Zu jeden $\varepsilon >0$ existiert ein $N=N(\varepsilon) \in \mathbb{N}$, sodass
    \[ a_n - a_m < \varepsilon \]
    für alle $n,m \geq N$.\\
    Schreibweise: $\lim_{n,m\to \infty} \abs{a_n-a_m}=0$, $\abs{a_n-a_m} \to 0 \enspace (n,m \to \infty)$.
\end{definition}

\begin{theorem}\label{theorem1}
    Jede konvergente Folge $(a_n)$ komplexer Zahlen ist eine Cauchy-Folge.
\end{theorem}

\begin{proof}
    Sei $\underset{n \to \infty}{lim} a_n =0$ und $\varepsilon >0$. Dann gibt es ein $N=N(\varepsilon)\in \mathbb{N}$ mit $\abs{a_n-a}<\frac{\varepsilon}{2}$ für alle $n \geq N$. Sind dann $n$ und $m \geq N$, folgt
    \[ \abs{a_n-a_m} \leq \abs{a_n-a} + \abs{a-a_m} < \frac{\varepsilon}{2} + \frac{\varepsilon}{2} = \varepsilon \text{.} \]
\end{proof}

\begin{definition}
    Das Vollständigkeitsaxiom für die reelen Zahlen besagt gerade, dass in $\mathbb{R}$ und die Umkehrung gilt:
    \begin{enumerate}
        \item[(V)] Jede Cauchy-Folge $(a_n)$ reeler Zahlen besitzt einen Grenzwert $a \in \mathbb{R}$.
    \end{enumerate}
\end{definition}

\begin{remark}
    Mit dem Vollständigkeitsaxiom ist die axiomatische Charakterisierung der reelen Zahlen abgeschlossen. Wir fassen also im folgenden $\mathbb{R}$ auf als einen vollständigen, archimedisch angeordneten Körper, in den die natürlichen Zahlen (und damit auch $\mathbb{Z}$ und $\mathbb{Q}$) eingebettet sind. Durch die damit geforderten Axiome
    \begin{enumerate}
        \item[(V)] -- Vollständigkeit
        \item[(A)] -- Archimedes
        \item[(A1)-(A3)] -- Anordnung
        \item[(K1)-(K9)] -- Körper
    \end{enumerate}
    sind die reelen Zahlen ``bis auf Isomorphie'' eindeutig bestimmt - insofern haben wir die reelen Zahlen tatsächlich charakterisiert. Einen Beweis der Eindeutigkeitsaussage finden Sie in\\
    Ebbinhaus u. a.: Zahlen, Kap.2, §5
    ``Eindeutig bis auf Isomorphie'' bedeutet in diesem Zusammenhang: Ist $K$ ein weiterer Körper, in dem alle o. g. Axiome gelten, so existiert eine Bijektion $\varphi : K \to \mathbb{R}$ mit
    \begin{itemize}
        \item $\varphi(0_K)=0$, $\varphi(1_K)=1$, $\varphi(a+b)=\varphi(a)+\varphi(b)$, $\varphi(ab)=\varphi(a)\varphi(b)$
        \item $a<b \enspace \Leftrightarrow \enspace \varphi(a)<\varphi(b)$
        \item $(a_n)$ konvergent bzw. Cauchy $\Leftrightarrow (\varphi(a_n))$ konvergent bzw. Cauchy
    \end{itemize}
\end{remark}

Im Rest dieses Abschnitts werden wir eine Reihe wichtiger Eigenschaften der reelen Zahlen aus dem Vollständigkeitsaxiom und den vorgegangenen Axiomen herleiten. Zunächst wollen wir uns jedoch davon überzeugen, dass (V) tatsächlich eine Eigenschaft ist, die $\mathbb{R}$ von $\mathbb{Q}$ unterscheidet. Dazu werden wir eine Cauchy-Folge rationaler Zahlen angeben, die in $\mathbb{Q}$ keinen Grenzwert besitzt. Zum Nachweis der Cauchy-Eigenschaften dient dabei das folgende Kriterium.

\begin{theorem}\label{theorem2}
    Es sei $(a_n)$ eine Folge komplexer Zahlen mit der folgenden Eigenschaft:\\
    Es existiert $C \geq 0$ und $q \in [0,1)$ sowie ein $n_0 \in \mathbb{N}$, sodass für $n \geq n_0$ gilt
    \[ \abs{a_{n+1}-a_n} \leq q \abs{a_n - a_{n-1}} \text{,} \]
    denn dann ist
    \[ \abs{a_{n+1}-a_n} \leq q \abs{a_n - a_{n-1}} \leq q^2 \abs{a_{n-1} - a_{n-2}} \leq ... \leq q^{n-n_0} \abs{a_{n_0+1} - a_{n_0}} = q^n \cdot C \\ \text{mit } C=q^{-n_0} \abs{a_{n_0+1} - a_{n_0}} \text{.} \]
\end{theorem}

\begin{proof}
    \begin{align*}
        \begin{autobreak}
            \abs{a_n-a_m}
            =\abs{\sum_{k=m}^{n-1} a_{k+1}-a_k}
            \leq \sum_{k=m}^{n-1}\abs{a_{k+1}-a_k} \quad \text{(Dreiecksungleichung)}
            \leq C \cdot \sum_{k=m}^{n-1}q^k
            =C \cdot q^m \cdot \sum_{k=0}^{n-m-1}q^k
            =C \cdot q^m \cdot \frac{1-q^{n-m}}{1-q}
            \leq C \cdot \frac{q^m}{1-q}
            \to 0 \enspace (m \to \infty)
        \end{autobreak}
    \end{align*}
    Zu vorgegebenen $\varepsilon>0$ finden wir also ein $N=N(\varepsilon)$, sodass für alle $m\geq n$ gilt
    \[ C\cdot \frac{q^m}{1-q} < \varepsilon \]
    und damit auch $\abs{a_n-a_m}<\varepsilon$ für alle $m,n\geq N$.
\end{proof}

\begin{example}
    Wir betrachten nun für $a>0$ die rekursiv definierte Folge
    \[ x_1=a \qquad x_{n+1}=\frac{1}{2}(x_n+\frac{a}{x_n}) \text{.}\]
    Wir stellen fest:
    \begin{enumerate}
        \item Für $a \in \mathbb{Q}$ sind alle $x_n \in \mathbb{Q^+}$.
        \item $x_n x_{n-1} \geq \frac{a}{2}$ (denn: $x_2x_1=\frac{a}{2}(a+1)\geq \frac{a}{2}$ und für $n\geq 3$ gilt \[x_n \cdot x_{n-1} = \frac{x_{n-1}}{2} (x_{n-1} + \frac{a}{x_{n-1}}) = \frac{{x_{n-1}}^2}{2} + \frac{a}{2} \geq \frac{a}{2}\text{ )}\]
        \item $x_{n+1}-x_n=\frac{1}{2}(x_n-x_{n-1}) + \frac{1}{2}(\frac{a}{x_n}-\frac{a}{x_{n-1}})=\frac{1}{2}(x_n-x_{n-1})(1-\frac{a}{x_n x_{n-1}})$\\
              Nach 1., 2. ist $0\leq \frac{a}{x_n x_{n-1}} \leq 2$ und damit $\abs{1-\frac{a}{x_n x_{n-1}}} \leq 1$, also $\abs{x_{n+1}-x_n} \leq \frac{1}{2}\abs{x_n - x_{n-1}}$.
        \item Nach Satz 2 (und der anschließenden Bemerkungen) ist $(x_n)$ eine Cauchy-Folge (rationaler Zahlen, falls $a\in \mathbb{Q}$, nach 1.)
        \item Aufgrund von (V) existiert der Grenzwert $\lim _{n\to \infty} x_n =: x \in \mathbb{R}$. Die Rechenregeln ergeben
              \[x=\lim _{n\to \infty} x_{n+1}=\lim _{n\to \infty} \frac{1}{2}(x_n+\frac{a}{x_n}) = \frac{1}{2}(\lim _{n\to \infty} x_n +\frac{a}{\lim _{n\to \infty} x_n}) = \frac{1}{2}(x+\frac{a}{x}) \text{.}\]
              Also $x^2=a$, der Grenzwert ist also die eindeutig bestimmte positive Lösung der Gleichung $x^2=a$, d. h. $x=\sqrt{a}$.
        \item Damit ist der Beweis der Existenz von Quadratwurzeln nchgeholt und zugleich ein Verfahren zu ihrer näherungsweisen Berechnung angeggeben, das sog. ``babylonische Wurzelziehen''.
        \item Für $a=2$ ist $x\notin \mathbb{Q}$, also \emph{$\mathbb{Q}$ nicht vollständig}.
              \begin{proof}{(aus dem Buch X der ``Elemente'' Euklids, ca. 300 v. Chr.)}\\
                  Annahme $x\in \mathbb{Q} \Rightarrow \exists p,q\in \mathbb{N}$, $p,q$ teilerfremd, sodass $x=\frac{p}{q}$\\
                  $\Rightarrow \enspace 2=\frac{p^2}{q^2}$, also $p^2=2\cdot q^2$, insbesondere $p^2$ gerade\\
                  $\Rightarrow \enspace p$ gerade (denn das Quadrat von ungerade ist ungerade!)\\
                  $\Rightarrow \enspace \exists r\in \mathbb{N}$ mit $p=2r$\\
                  $\Rightarrow \enspace 4r^2=p^2=2q^2 \enspace \Rightarrow \enspace q^2=2r^2 \enspace \Rightarrow \enspace q^2$ gerade\\
                  $\Rightarrow \enspace q$ gerade. Widerspruch zur Teilerfremdheit!
              \end{proof}
    \end{enumerate}
\end{example}

Im folgenden solle die wesentlichen Eigenschaften von $\mathbb{R}$ aus den Axiomen hergeleitet werden. Wir beginnen mit dem wichtigen Satz von Bolzano-Weierstraß:

\begin{theorem}{(Bolzano-Weierstraß)}\label{theorem3}
    Jede beschränkte Folge $(a_n)_{n\in \mathbb{N}}$ reeler Zahlen besitzt eine konvergente Teilfolge $(a_{n_k})_{k\in \mathbb{N}}$.
\end{theorem}

\begin{proof}\hfill
    \begin{enumerate}
        \item Da $(a_n)$ beschränkt ist, existieren Zahlen $A_0$ und $B_0$ mit $A_0\leq a_n\leq B_0$ für alle $n\in \mathbb{N}$. Hiervon ausgehend konstruieren wir rekursiv eine folge von Intervalen $[A_k,B_k] \enspace (k\geq 0)$ mit den folgenden Eigenschaften:
            \begin{enumerate}[label=(\roman*)]
                  \item $[A_{k+1},B_{k+1}] \subset [A_k,B_k]$ für alle $k\geq 0$
                  \item $B_k-A_k=(B_0-A_0)\cdot 2^{-k}$ für alle $k\geq 0$
                  \item in jedem $[A_k,B_k]$ liegen uendlich viele Folgenglieder, also $\#\{a_n:n\in \mathbb{N}\} \cap [A_k,B_k] = \infty$.
            \end{enumerate}
              Für $k=0$ sind die Eigenschaften (ii) und (iii) offenbar erfüllt. Wir wählen $[A_1,B_1]=[\frac{A_0+B_0}{2},B_0]$, falls $\#[\frac{A_0+B_0}{2},B_0]\cap\{a_n:n\in \mathbb{N}=\infty\}$, anderenfalls $[A_1,B_1]=[A_0,\frac{A_0+B_0}{2}]$.
              Sei nun $[A_0,B_0],[A_1,B_1],\dots,[A_k,B_k]$ bereits gewählt, sodass die Bedingungen (ii) und (iii) und (i) für jedes $j\leq k-1$ erfüllt sind, so setzen wir $[A_{k+1},B_{k+1}]=[\frac{1}{2}(A_k+B_k), B_k]$, falls hierin unendlich viele $(a_n)$ enthalten sind, anderenfalls $[A_{k+1},B_{k+1}]=[A_k,\frac{1}{2}(A_k+B_k)]$.
              Damit ist eine Folge von Intervallen mit den Eigenschaften (i) bis (iii) rekursiv definiert.
        \item Auswahl einer Teilfolge: Wir wählen $a_{n_1}=a_1$ und, wenn $a_{n_1},\dots,a_{n_k}$ bereits bestimt sind,
              \[ a_{n_{k+1}}\in \{a_n:n>n_k\}\cap [A_{k+1},B_{k+1}] \text{.} \]
              Ist dann $l\geq k$, so gilt nach (i)
              \[ a_{n_k}, a_{n_l} \in [A_k,B_k] \]
              und nach (ii)
              \[ \abs{a_{n_k}- a_{n_l}} \leq \abs{B_0-A_0} \cdot 2^{-k} \text{.} \quad \to 0 \enspace (k \to \infty) \qquad \text{((A)!)} \]
              Also: $\lim_{k,l\to\infty}{a_{n_k}- a_{n_l}}=0$. Das bedeutet: Die Teilfolge $(a_{n_k})_{k\in \mathbb{N}}$ von $(a_n)$ ist eine Cauchy-Folge, die wegen (V) in $\mathbb{R}$ konvergiert.
    \end{enumerate}
\end{proof}

\begin{remark}
    Ohne das archimedische Axiom (A) bricht nicht nur unser Beweis zusammen, sondern auch die Aussage des Satzes wird falsch. Denn:
    Gilt (A) nicht, ist die Folge $(nx)_{n\in \mathbb{N}}$ für ein $x>0$ beschränkt. Wegen $\abs{nx-mx}=\abs{n-m}x\geq x$ für $x\neq m$, besitzt $(nx)_n$ keine Teilfolge, die Cauchy-Folge ist, also auch keine konvergente Teilfolge.
    (Unter der Voraussetzung (K1)-(K9), (A1)-(A3) impliziert also der Satz von Bolzano-Weierstraß das archimedische Axiom!)
\end{remark}

Eine wichtige Folgerung aus den Satz von Bolzano-Weierstraß ist das nachstehende Konvergenzkriterium, für Folgen, das auf der Anordnung von$\mathbb{R}$ beruht.

\begin{theorem}\label{theorem4}
    Jede nach oben beschränkte, monoton steigende Folge $(a_n)$ reeler Zahlen ist konvergent.
\end{theorem}

\begin{proof}
    Nach Voraussetzungen existiert $C\in \mathbb{R}$ mit $a_1\leq a_n\leq 0$ für alle $n\in \mathbb{N}$, d. h. $(a_n)$ ist beschränkt und besitzt nach Bolzano-Weierstraß (mindestens) einen Häufungswert.
    Nehmen wir an, es gibt zwei (oder mehr) Häufungswert $a$ und $b$ mit $a<b$, so setzen wir $\varepsilon := \frac{b-a}{2}>0$. Dann existiert $n_0$ mit $a_{n_0}>b-\varepsilon$ und aufgrund der Monotonie gilt $a_n>b-\varepsilon$ für alle $n\geq n_0$.
    Aufgrund unserer Wahl von $\varepsilon$ kann $\abs{a_n-a}<\varepsilon$ (mit? wir?) für solche $n$ mit $n\leq n_0$ gelten, also für endich viele. Dies steht im Widerspruch zur Annahme, $a$ sei ein Häufungswert.
    %(mit? wir?) unerkennbar
\end{proof}

\begin{deduction}\label{deduction1}
    Jede nach unten beschränkte, monoton fallende Folge $(b_n)$ ist konvergent.
    \begin{proof}
          Wende \autoref{theorem4} an auf $(a_n)$ mit $a_n=-b_k$.
    \end{proof}
\end{deduction}

Fassen wir die Aussagen von \autoref{theorem4} und \autoref{deduction1} zusammen, erhalten wir die einprägsame Formulierung

\begin{deduction}\label{deduction2}
    Jede beschränkte monotone Folge reeler Zahlen ist konvergent.
\end{deduction}

Die im Beweis des Satzes von Bolzano-Weierstraß verwendete Folge von Intervallen nennt man eine Intervallschachtelung. Genauer

\begin{definition}
    Eine Folge $(J_n)_{n\in \mathbb{N}}$ abgeschlossener Intervalle $J_n=[A_n,B_n]$ heißt eine \emph{Intervallschachtelung}, falls
    \begin{enumerate}
        \item $J_{n+1}\circ J_n \quad \forall n\in \mathbb{N}$ \quad (``$(J_n)_{n\in \mathbb{N}}$ ist absteigend'')
        \item $\lim_{n\to \infty}{B_n-A_n}=0$.
    \end{enumerate}
\end{definition}

 Als eine weitere Folgerung aus den Vollständigkeitsaxiom (V) erhalten wir:

 \begin{theorem}[Intervallschachtelungsprinzip]\label{theorem5}
     $(J_n)_{n\in \mathbb{N}}$ sei eine Intervallschachtelung. Dann existiert genau eine Zahl $c\in \mathbb{R}$ mit $c\in \bigcap_{n\in \mathbb{N}} J_n$.
 \end{theorem}

\begin{proof}
    \begin{enumerate}
        \item Eindeutigkeit: Sind $c$ und $c'$ in $\bigcap_{n\in \mathbb{N}} J_n$, so gilt $\abs{c-c'} \leq B_n-A_n$ für alle $n\in \mathbb{N}$. Da $\lim_{n\to \infty}{B_n-A_n}=0$, folgt $\abs{c-c'}=0$, also $c=c'$.
        \item Existenz: Wir haben $[A_n,B_n]=J_n \supset J_{n+1}=[A_{n+1},B_{n+1}]$, also $A_1\leq \dots \leq A_n \leq A_{n+1} < B_{n+1} \leq B_n \leq \dots \leq B_1$.
        Die Folge $(A_n)_{n\in \mathbb{N}}$ ist also monoton steigend und nach oben beschränkt, daher nach \autoref{theorem4} konvergent. Also existiert
        \[ \lim_{n\to \infty} A_n =: A \]
        und, mit einem ähnlichen Argument,
        \[ \lim_{n\to \infty} B_n =: B \text{.} \]
        Wegen $A_n \leq B_n$ folgt $A\leq B$ (Exkurs, Lemma 4) und daher aufgrund der Monotonie
        %autoref für Exkurs, Lemma 4
        \[A_n \leq A \leq B \leq B_n \quad \forall n\in \mathbb{N} \text{.} \]
        $\Rightarrow A \in \bigcap_{n\in \mathbb{N}}[A_n,B_n]=\bigcap_{n\in \mathbb{N}} J_n$.
    \end{enumerate}
\end{proof}

\begin{example}[zu den Sätzen \autoref{theorem4} und \autoref{theorem5}] Approximation der Eulerschen Zahl $e$.\\
    Wir betrachten die Folge $(e_n)_{n\in \mathbb{N}}$ mit $e_n = (1+ \frac{1}{n})^n$.
    Die ersten Folgenglieder sind gegeben durch $e_1=(1+1)^2=2$, $e_2=(1+\frac{1}{2})^2=\frac{9}{4}=2,25$ und $e_3=(1+\frac{1}{3})^3-(\frac{3}{4})^3=\frac{64}{27}= 2,{\overline {370}}$.
    $\leadsto$ Vermutung: $e_n$ monoton steigend. Dazu schätzen wir den Quotienten $\frac{e_n}{e_{n-1}}$ nach unten ab:
    \begin{align*}
        \begin{autobreak}
            \frac{e_n}{e_{n-1}}
            =\frac{(1+\frac{1}{n})^n}{(1+\frac{1}{n-1})^{n-1}}
            =(1+\frac{1}{n})\cdot (\frac{\frac{n+1}{n}}{\frac{n}{n-1}})^{n-1}
            =(1+\frac{1}{n})\cdot (\frac{n^2-1}{n^2})^{n-1}
            =(1+\frac{1}{n})(1- \frac{1}{n^2})^{n-1}
            \underset{\text{Bernoulli}}{\geq}(1+\frac{1}{n})(1-\frac{n-1}{n^2})
            =(1+\frac{1}{n})(1-\frac{1}{n}+\frac{1}{n^2})
            =1-\frac{1}{n^2}+\frac{1}{n^2}+\frac{1}{n^3} \geq 1
            \Rightarrow e_n \geq e_{n-1}
        \end{autobreak}
    \end{align*}
    Anderenseits ist $e_n\leq (1+\frac{1}{n})e_n=(1+\frac{1}{n})^{n+1}=:{e_n}^*$ und die Folge $({e_n}^*)$ ist monoton fallend (Beweis in den Übungen). Damit:
    \[ 2=e_1\leq \dots \leq e_n \leq{e_n}^* \leq \dots \leq {e_1}^*=4 \]
    %im Skript fehlt \leq zwischen \dots und {e_1}^*
    insbesondere: $(e_n)$ monoton steigend und nach oben durch ${e_1}^*=4$ beschränkt, also nach Satz 4 konvergent.
    Man definiert jetzt die Eulersche Zahl durch
    \[ e:= \lim_{n\to \infty} e_n \qquad (e=2,7182818\dots) \]
    Zugleich haben wir hier ein Beispiel für eine Intervallschachtelung, denn $J_n:=[e_n, {e_n}^*]$ bilden eine Folge abgechlossener Invervalle mit $J_{n+1}\subset J_n$, da $e_n \leq e_{n+1}<{e_{n+1}}^*\leq {e_n}^*$.
    Außerdem: ${e_n}^*-e_n=(1+\frac{1}{n})e_n-e_n=\frac{1}{n}\cdot e_n \leq \frac{4}{n}$ (s. o.), also ist auch die zweite Eigenchaft einer Intervallschachtelung gegeben, und es gilt
    \[ e \in \bigcap_{n\in \mathbb{N}} \left[e_n, {e_n}^* \right] \text{.} \]
\end{example}

In recht engen Zusammenhang mit der Rechnung oben steht der erste der beiden folgenden Grenzwerte:

\begin{example}
    \begin{enumerate}[label=(\alph*)]
        \item \[\lim_{n\to \infty}\sqrt[n]{n!} = \infty\]
        \item \[\lim_{n\to \infty}\sqrt[n]{n} = 1\]
    \end{enumerate}
\end{example}
%inline enumerate (environment enumerate*) mit \usepackage[inline]{enumitem} geht nicht, ich lasse es zuerst mal so

\begin{proof}
    \begin{enumerate}[label=Zu (\alph*)]
        \item reicht es zu zeigen, dass $(\frac{n}{4})^n\leq n!$. Dies sieht man durch Induktion über $n$, für $n=1$ lautet die Behuptung $\frac{1}{4}\leq 1$, was offenbar erfüllt ist.
        Induktionsschritt: $n\to n+1$
        \[ \left( \frac{n+1}{4}\right) ^{n+1}=\frac{n+1}{4}\cdot \left( \frac{n+1}{n} \right) ^n \]
        \item Wir setzen $x_n=\sqrt[n]{n}-1$
        \begin{align*}
            &\curvearrowright n=(1+x_n)^2 =\sum_{k=0}^n{\binom{n}{k} {x_n}^n} \geq\binom{n}{2} {x_n}^2 =\frac{n(n-1)}{2}{x_n}^2\\
            &\curvearrowright x_n \leq \sqrt{\frac{2}{n-1}} \to 0 \enspace (n\to \infty) \text{.}
        \end{align*}
    \end{enumerate}
\end{proof}

Eine weitere Konsequenz aus \autoref{theorem4} ist die Folgende charakteristische Eigenschaft der reelen Zahlen:

\begin{theorem}\label{theorem6}
    Jede nach oben beschränkte Teilfolge $E\neq \emptyset$ von $\mathbb{R}$ besitzt in $\mathbb{R}$ ein Supremum.
\end{theorem}


\begin{proof}
    Sei S eine obere Schranke von $E$ und
    \[ A_0 := \{S-k:k\in \mathbb{N}_0 \text{ und } S-k\geq x \text{ für alle } x\in E \} \text{.} \]
    Dann ist $A_0$ eine endliche Menge, besitzt also ein Minimum
    \[ a_0:=\min A_0 \text{.} \]
    Nun definieren wir rekursiv
    \begin{align*}
        A_{n+1}&:=\{a_n-\frac{k}{2^{n+1}}:k\in \mathbb{N} \text{ und } a_n-\frac{k}{2^{n+1}}\geq x \quad \forall x \in E\} \text{,}\\
        a_{n+1}&:=\min (A_{n+1}) \quad \text{(auch $\#A_{n+1}<\infty$!)}
    \end{align*}
    Diese Wahl ergibt eine Folge $(a_n)$ mit den folgenden Eigenschaften:
    \begin{enumerate}
        \item $a_{n+1}\leq a_n \quad \forall n\in \mathbb{N}_0$,
        \item $a_n\geq x \quad \forall n\in \mathbb{N}_0 \text{, } x\in E$,
        \item $\forall n\in \mathbb{N} \quad \exists x_n\in E$ mit $a_n-\frac{1}{2^n}\leq x_n$.
    \end{enumerate}
    Nach 1. und 2. ist die Folge $(a_n)$ also monoton fallend und nach unten beschränkt. Mit \autoref{deduction1} aus \autoref{theorem4} erhalten wir die Existenz von
    \[ a:=\lim_{n\to \infty}a_n \text{.} \]
    Hierfür gilt $a\geq x$ für jedes $x\in E$, denn wir haben $a_n\geq x \quad n\in \mathbb{N}\text{, } x\in E$. D. h. $a$ ist eine obere Schranke von $E$. Ferner gilt\\
    Zu jeden $n\in \mathbb{N}$ existiert $x_n\in E$,sodass $a-\frac{1}{2^n}\leq a_n-\frac{1}{2^n}\leq x_n$. Also kann es keine kleinere obere Schranke von $E$ geben.
\end{proof}

\begin{deduction}
    Jede nichtleere, nach unten beschränkte Teilmenge $E'\subset \mathbb{R}$ besitzt in $\mathbb{R}$ ein Infimum.
\end{deduction}

\begin{proof}
    Wende \autoref{theorem6} an auf $E=-E'=\{-x:x\in E'\}$.
\end{proof}

In diesem Zusammenhang wird häufig die folgende \emph{Konnvention} benutzt:

\begin{enumerate}
    \item $\sup A:=\infty$, falls $A\subset \mathbb{R}$ nach oben, $\inf A=-\infty$, falls $A\subset \mathbb{R}$ nach unten beschränkt ist;
    \item $\sup \emptyset :=-\infty$, $\inf \emptyset :=\infty$.
\end{enumerate}

Mit \autoref{theorem6} und dieser Konvention sind Supremum und Infimum für jede Teilmenge von $\mathbb{R}$ definiert.
