\chapter{Logikregeln und Quantoren}

\paragraph{Stichworte:} Implikation $\Rightarrow$, Äquivalenz $\Leftrightarrow$, Logikregeln, direkter und indirekter Beweis, Prädikat, Quantoren $\forall$ $\exists$, Verneinung/Umgang mit Quantoren

% ABSCHNITT 2.1
\section{Allgemeines}

Die \emph{Implikation/Schlussfolgerung/Folgerung} zweier Aussagen A und B ist die Aussage $\lnot A\lor B$ und wird bezeichnet mit dem Symbol $A \Rightarrow B$, sprich ''aus A folgt B'', ''A impliziert B'', ''wenn A gilt, dann gilt B'', ''A ist hinreichend für B'', ''B ist notwendig für A'', ''damit B gilt, ist hinreichend, dass A gilt'', ''wenn A gilt, muss B notwendig auch gelten'', ... und hat die Wahrheit laut Tabelle: \\

\begin{center}
    \begin{tabular}{c | c | c}
      $A$ & $B$ & $A \Rightarrow B$ bzw. $\lnot A \lor B$ \\
      \hline
      w & w & w\\
      w & f & f\\
      f & w & w\\
      f & f & w
    \end{tabular}
\end{center}
Laut Bedeutung von $\lnot A \lor B$ gilt:

Ist A wahr, also $\lnot$ A falsch, dann muss B stimmen, damit die Aussage insgesamt stimmt. Damit wurde ''wenn A wahr ist, dann gilt B'' ausgesagt, also eine Folgerung ausgedrückt. Ist A falsch, ist $\lnot A \lor B$ wahr, egal, was B ist: ''ex falso quodlibet'' = ''aus Falschem folgt Beliebiges''.

Wir wollen ''ex falso quodlibet'' so zulassen: wenn wir es anders machen wollten, also z.B. ''$A \Rightarrow B$'' für falsch erklären, falls A falsch und B wahr/falsch ist hätte man andere Aussagen erklärt und nichts Neues, vgl. diese Tabelle:

\begin{center}
    \begin{tabular}{c | c | c | c | c}
      $A$ & $B$ & $(A\land B)\lor(\lnot A\land\lnot B)$ & $B$ & $A\land B$\\
      \hline
      w & w & w & w & w\\
      w & f & f & f & f\\
      f & w & f & w & f\\
      f & f & w & f & f
    \end{tabular}
\end{center}

Aber auch sonst ist ''es falso quodlibet'' ein nützliches Prinzip: soll ''$\Rightarrow$'' Bestandteil von Formeln werden, ist es dann mit unserem Einsetzprinzip kompatibel: Die Formel ''$\forall x \in \mathbb{R} : x>3\Rightarrow x^{2}>$'' ist richtig, egal welche reelle Zahl für x eingesetzt wird, etwa\\
$x=5 : 5>3\Rightarrow 5^{2}>9, \  x=2 : 2>3\Rightarrow 2^{2}>9, \  x=-4 : -4>3\Rightarrow (-4)^{2}>9$

% ABSCHNITT 2.2
\section{Äquivalenz}

Die \emph{Äquivalenz} zweier Aussagen ist die Aussage $(A\Rightarrow B)\land (B\Rightarrow A)$, d.h. sie ist wahr genau dann, wenn A und B dieselben Wahrheitserte besitzen. Wir schreiben $A\Rightarrow B$ für diese Aussage und lesen dafür ''A ist genau dann wahr, wenn B gilt'', ''A gilt genau dann, wenn B gilt'', ''A gdw. B'', ''A ist dann und nur dann wahr, wenn B wahr ist'', ''A ist äquivalent zu B'', ''A ist notwendig und hinreichend für B'', ...

\begin{center}
    \begin{tabular}{c | c | c}
      $A$ & $B$ & $A \Leftrightarrow B$\\
      \hline
      w & w & w\\
      w & f & f\\
      f & w & f\\
      f & f & w
    \end{tabular}
\end{center}

Anhand der Wahrheitstabelle ist erkennbar, dass $A\Leftrightarrow B$ dieselben Wahrheitswerte wie $(A\land B)\lor (\lnot A\land B)$ hat; wir werden dies gleich noch auf anderen Wege sehen.

% ABSCHNITT 2.3
\section{Pfeilrichtungen}

Noch ein Paar Begriffe in diesem Zusammenhang: in einer Äquivalenz $A\Leftrightarrow B$ heißt $A \Rightarrow B$ die \emph{Hinrichtung}, und $B\Rightarrow A$, was auch als $A\Leftarrow B$ geschrieben werden kann, die \emph{Rückrichtung}. Wenn eine Implikation $A\Rightarrow B$ vorliegt, wie z.B. $x>2\Rightarrow x^{2}>4$, muss noch lange nicht die Rückrichtung gelten. Oft wird dies gefragt, da Äquivalenzen eine genauere Aussage ermöglichen, wie z.B. $(x>2\lor x<-2)\Leftrightarrow x^{2}>4$.

Zum Sparen von Klammern gibt es für $\Rightarrow$, $\Leftrightarrow$ folgende Klammersetzungsregeln:

\begin{center}
    $\lor$ bindet stärker als $\Rightarrow$ bzw. $\Leftarrow$,\\
    $\Rightarrow$ bzw. $\Leftarrow$ bindet stärker, als $\Leftrightarrow$
\end{center}

Weiter lassen Äquivalenzen die Formulierung zu, dass zwei Formeln in Aussagenvariablen dieselben Wahrheitswerte haben, z.B. in $(A\Rightarrow B)\Leftrightarrow (\lnot A\lor B).$\\

Diese nutzen wir jetzt, um wichtige Logikregeln zu formulieren und auch zu beweisen.

% ABSCHNITT 2.4
\section{Satz}

Es gelten die folgenden \emph{Logikregeln} für beliebige Aussagen A,B,C:

\begin{itemize}
    \setlength\itemsep{0em}
    \item[(1)] $A\Leftrightarrow\lnot(\lnot A)$
    \item[(2)] $(A\land B)\land C\Leftrightarrow A\land(B\land C)$
    \item[(3)] $(A\lor B)\lor C\Leftrightarrow A\lor (B\lor C)$
    \item[(4)] $\lnot(A\land B)\Leftrightarrow\lnot A\lor\lnot B$
    \item[(5)] $\lnot(A\lor B)\Leftrightarrow\lnot A\land\lnot B$
    \item[(6)] $A\land(B\lor C)\Leftrightarrow(A\land B)\lor(A\land C)$
    \item[(7)] $A\lor(B\land C)\Leftrightarrow(A\lor B)\land(A\lor C)$
\end{itemize}
\textbf{Beweis}: \\
\hangindent=15pt\hangafter=2\emph{Zu(1)}: Ist A wahr, dann ist $\lnot A$ falsch, also $\lnot(\lnot A)$ wieder wahr. Ist A falsch, dann ist $\lnot A$ wahr, also $\lnot(\lnot A)$ wieder falsch.
Also haben A und $\lnot(\lnot A)$ dieselben Wahrheitswerte, egal was A ist.

\emph{Zu (2)\&(3)}: Sind ebenso klar, auch anhand der Wahrheitstafeln.
\\
\hangindent=15pt\hangafter=2\emph{Zu (4)}: Sind A,B beide wahr, ist $\lnot(A\land B)$ falsch und ebenso $\lnot A\lor \lnot B$.\\
Sind A,B beide falsch, ist $\lnot(A\land B)$ wahr und ebenso $\lnot A\lor \lnot B$.\\
Haben A,B verschiedene Wahrheitswerte, ist $\lnot(A\land B)$ wahr und ebenso $\lnot A\lor\lnot B$.\\
In jedem Fall haben $\lnot(A\land B)$ und $\lnot A\lor\lnot B$ dieselben Wahrheitswerte, egal was A,B ist.


\hangindent=15pt\hangafter=2\emph{Zu (5)}: Haben $\lnot(A\lor B)
\overset{\text{(1)}}{\implies}   \lnot(\lnot(\lnot A) \lor \lnot(\lnot B))
\overset{\text{(4)}}{\implies}    \lnot(\lnot(\lnot A\land\lnot B))
\overset{\text{(1)}}{\implies}   \lnot A\land\lnot B$.

\hangindent=15pt\hangafter=2\emph{Zu (6)}: Checken der Wahrheitswerte fällt am leichtesten:

\begin{center}
    \begin{tabular}{c | c | c | c | c | c | c | c}
      $A$ & $B$ & $C$ & $B\lor C$ & $A\land(B\lor C)$ & $A\land B$ & $A\land C$ & $(A\land B)\lor (A\land C)$\\
      \hline
      w & w & w & w & w & w & w & w\\
      w & w & f & w & w & w & f & w\\
      w & f & w & w & w & f & w & w\\
      w & f & f & f & f & f & f & f\\
      f & w & w & w & f & f & f & f\\
      f & w & f & w & f & f & f & f\\
      f & f & w & w & f & f & f & f\\
      f & f & f & f & f & f & f & f\\
    \end{tabular}
\end{center}

\hangindent=15pt\hangafter=1\emph{Zu (7)}: $A\lor(B\land C)
\overset{\text{(1)}}{\iff}   \lnot(\lnot(A\lor(B\land C)))
\overset{\text{(5)}}{\iff}   \lnot(\lnot A\land\lnot(B\land C))
\overset{\text{(4)}}{\iff}   \lnot(\lnot A \land (\lnot B\lor \lnot C))
\overset{\text{(6)}}{\iff}   \lnot((\lnot A\land\lnot B)\lor(\lnot A\land\lnot C))
\overset{\text{(5)}}{\iff}   \lnot(\lnot A\land\lnot B)\land\lnot(\lnot A\land\lnot C)
\overset{\text{(4)}}{\iff}   (\lnot(\lnot A)\lor\lnot B)\land(\lnot(\lnot A)\lor\lnot(\lnot C))
\overset{\text{(1)}}{\iff}   (A\lor B)\land(A\lor)\quad\square$.

\paragraph{$\square$} = Zeichen für ein Beweisende, auch ''q.e.d.'' = ''quod erat demonstrandum'', oder ''wzbw'' = ''wie zu beweisen war''

% ABSCHNITT 2.5
\section{}
Dies war schon unser erster Beweis. Beachten Sie, dass wir für Regeln (5) und (7) im Beweis vorangehende Ergebnisse benutzt haben, was sehr elegant ist: Wir müssen derart nicht mehr die Wahrheitstabellen durchgehen. Dabei ist für Sie nützlich, dass ich die verwendeten Regeln dazugenannt habe. Für (6) habe ich keinen Einfacheren Beweis, der nur mit $\lnot$,$\lor$,$\land$ auskommt, gefunden.

% ABSCHNITT 2.6
\section{}

Nun wollen wir noch Logikregeln mit ''$\Rightarrow$'' und ''$\Leftrightarrow$'' aufstellen.\\ Zunächst halten wir folgende Umschreibung fest:
\begin{itemize}
    \setlength\itemsep{0em}
    \item $(A\Rightarrow B)   \overset{\text{}}{\iff}   \lnot A\lor B   \overset{\text{(1)}}{\iff}   \lnot A\lor\lnot(\lnot B)   \overset{\text{(1)}}{\iff}   \lnot(A\land\lnot B)$\\
    \item $\begin{alignedat} {1}
                (A\Leftrightarrow B)   &\overset{\text{Def.}}{\iff}   (A\Rightarrow B)\land(B\Rightarrow A)\\
                &\overset{\text{Def.}}{\iff}   (\lnot A\lor B)\land(\lnot B\lor A)\\
                &\overset{\text{}}{\iff}   (\lnot A\lor B)\land\lnot B\lor(\lnot A\lor B)\land A\\
                &\overset{\text{Distr.}}{\iff}   \lnot A\land\lnot B\lor B\land\lnot B\lor\lnot A\land A\lor B\land A\\
                &\overset{\text{}}{\iff}   (\lnot A\land\lnot B)\lor(A\land B).
        \end{alignedat}$
\end{itemize}
Die letzte Zeile interpretieren wir als ''A und B haben beide denselben Wahrheitswert.''

%ABSCHNITT 2.7
\section{Satz:} Es gelten die \emph{Logikregeln} für beliebige Aussagen A,B,C:
\begin{itemize}
    \setlength\itemsep{0em}
    \item[(1)] {\makebox[6cm]{$(A\Rightarrow B)\land A\Rightarrow B$\hfill}}modus ponens
    \item[(2)] {\makebox[6cm]{$(A\Rightarrow B)\Leftrightarrow(\lnot B\Rightarrow\lnot A)$\hfill}}Kontrapositionsregel
    \item[(3)] {\makebox[6cm]{$(A\Rightarrow B)\land\lnot B\Rightarrow\lnot A$\hfill}}modus tollens
    \item[(4)] {\makebox[6cm]{$(A\Rightarrow B)\land(B\Rightarrow C)\Rightarrow(A\Rightarrow C)$\hfill}}Transitivität von ''$\Rightarrow$''
    \item[(5)] {\makebox[6cm]{$(A\Leftrightarrow B)\land(B\Leftrightarrow C)\Rightarrow(A\Leftrightarrow C)$\hfill}}Transitivität von ''$\Leftrightarrow$''
\end{itemize}
Vor dem Beweis die \emph{Bedeutung/Interpretation} dieser Regeln:

\hangindent=15pt\hangafter=1\emph{Zu (1)}: Das ist die ''Schlussfolgerung'' schlechthin: Ist $A\Rightarrow B$ bewiesen, d.h. B wahr unter der \emph{Annahme}, dass A wahr sei, und ist A wahr, muss auch B wahr sein. Man sollte aso sagen, dass die \emph{Behauptung} B wahr ist unter der \emph{Voraussetzung} A.

\hangindent=15pt\hangafter=1\emph{Zu (2)}: Die Begründung von ''$A\Rightarrow B$'' kann \emph{indirekt} erfolgen, wenn ein Beweis der Richtigkeit von $\lnot B\Rightarrow\lnot A$ vorliegt, wie folgt:\\
''$A\Rightarrow B$ gilt, denn wenn ansonsten B falsch wäre (Achtung, Konjunktiv!), dann wäre auch schon A falsch gewesen (schon wieder Konjunktiv!). Wir haben aber die Annahme A für $A \Rightarrow B$ gemacht, daher kann also B nicht falsch sein.''\\
Bsp.: ''Wenn es regnet, dann ist die Straße nass'' ist logisch gleichbedeutend zu ''Wenn die Straße trocken ist, dann regnet es nicht.''

\hangindent=15pt\hangafter=1\emph{Zu (4)}: Die \emph{Schlusskette} $(A\Rightarrow B)\land(B\Rightarrow C)$ zeigt $A\Rightarrow C$.

Diese schreibt man kurz als $A\Rightarrow B\Rightarrow C$.

!!! Dies ist eine andere Aussage als $(A\Rightarrow B)\Rightarrow C$ oder $A\Rightarrow(B\Rightarrow C)$!!!

\hangindent=15pt\hangafter=1\emph{Zu (5)}: Analoges gilt für \emph{Äquivalenzketten} $A\Leftrightarrow B\Leftrightarrow$. Vgl. Beweise oben!\\


\textbf{Beweis des Satzes 2.7:}

\hangindent=15pt\hangafter=1\emph{Zu (1)}: Denn in den Zeilen der Wahrheitstabelle für $A\Rightarrow B$, wo $A\Rightarrow B$ wahr ist und ebenso a wahr (das ist dort nur die 1.Zeile!), ist auch B wahr.\\ Oder so: \\
$((A\Rightarrow B)\land A\Rightarrow B)
\Leftrightarrow  (\lnot(A\Rightarrow B)\lor\lnot A\lor B)
\Leftrightarrow  (\lnot(\lnot A\lor B)\lor\lnot A\lor B))
\Leftrightarrow  (A\land\lnot B)\lor\lnot A\lor B
\Leftrightarrow  (A\land\lnot B)\lor\lnot(A\land\lnot B)$, ist immer wahr.

\hangindent=15pt\hangafter=1\emph{Zu (2)}: $(A\Rightarrow B)  \Leftrightarrow  (\lnot A\lor B)
\Leftrightarrow  (B\lor\lnot A)
\Leftrightarrow  \lnot(\lnot B)\lor\lnot A
\Leftrightarrow  (\lnot B\Rightarrow\lnot A)$.

\hangindent=15pt\hangafter=1\emph{Zu (3)}: $(A\Rightarrow B)\land\lnot B  \overset{\text{(2)}}{\iff}  (\lnot B\Rightarrow\lnot A)\land\lnot B
\overset{\text{(1)}}{\implies}  \lnot A$

\hangindent=15pt\hangafter=1\emph{Zu (4)}: $((A\Rightarrow B)\land(B\Rightarrow C)\Rightarrow(A\Rightarrow C))
\Leftrightarrow  \lnot((\lnot A\lor B)\land (\lnot B\lor C))\lor(\lnot A\lor C)
\Leftrightarrow  A\land\lnot B\lor B\land\lnot C\lor\lnot A\lor C
\Leftrightarrow  (A\land\lnot B\lor\lnot A)\lor(B\land\lnot C\lor C)
\Leftrightarrow  ((A\lor\lnot A)\land(\lnot B\lor\not A))\lor((\lnot C\lor C)\land(B\lor C))
\Leftrightarrow  \lnot B\lor\lnot A\lor B\lor C
\Leftrightarrow  (B\lor\lnot B)\lor(\lnot A\lor C)$, ist immer wahr.

\hangindent=15pt\hangafter=1\emph{Zu (5)}: $(A\Leftrightarrow B)\land(B\Leftrightarrow C)\Rightarrow gilt,
denn (A\Rightarrow B)\land(B\Rightarrow A)\land(B\Rightarrow C)\land(C\Rightarrow B)
\Leftrightarrow  (A\Rightarrow B)\land(B\Rightarrow C)\land(C\Rightarrow B)\land(B\Rightarrow A)
\overset{\text{(4)}}{\implies}  (A\Rightarrow C)\land(C\Rightarrow A)
\Leftrightarrow  (A\Leftrightarrow C). \quad\square$

\paragraph{Bemerkung:} Haben wir hier nicht die zu zeigenden Aussagen beim Beweisen bereits verwendet? Ein berechtigter Einwand! Generell darf keine Vermischung von Objekt- und Metasprache erfolgen, um die Beweise zu führen. Dies könnte man noch sauber argumentieren, was wir der Übersichtlichkeit halber lassen.

% ABSCHNITT 2.8
\section{Kurze Theorie mathematischer Beweise:} Bewiesen werden soll ein Satz, der als Implikation formuliert wurde, d.h. in der Form: ''Satz: $A\Rightarrow B$'' Dabei heißt A die \emph{Voraussetzung}, und B die \emph{Behauptung} des Satzes. Man unterscheidet die folgenden zwei Arten von Beweisen:

\hangindent=15pt\hangafter=1\emph{1.} \emph{direkter Beweis:} Angabe einer Schlusskette $A\Rightarrow C_1\Rightarrow C_2\Rightarrow ...\Rightarrow C_n\Rightarrow B$

\hangindent=15pt\hangafter=1\emph{2.} \emph{indirekter Beweis:} Hier gibt es wieder zwei Arten:\\

\begin{itemize}
  \item \emph{Kontrapositionsbeweis}: direkter Beweis von $\lnot B\Rightarrow\lnot A$
  \item \emph{Wiederspruchsbeweis}: direkter Beweis von $A\land B\Rightarrow C$, wobei C falsch wie z.B. $A\land\lnot A$, $o=1$,...
\end{itemize}

- Dies zeigt $A\Rightarrow B$, da $(A\land\lnot B\Rightarrow C)\Leftrightarrow(\lnot(A\land\lnot)\lor C)\Leftrightarrow(A\Rightarrow B).$\\
- Wenn $C=\lnot A$ ist ebenso, da $(A\land\lnot B\Rightarrow\lnot A)   \overset{\text{Def.}}{\iff}   (\lnot(A\land\lnot B)\lor\lnot A)   \overset{\text{}}{\iff}  t (\lnot A\lor B)   \overset{\text{Def.}}{\iff}   (A\Rightarrow B)$\\

Gerade das indirekte Schließen in Wiederspruchsbeweisen werden wir noch genauer in Beispielen behandeln, speziell auch Tipps zum Aufschreiben geben.

\paragraph{Prädikatenlogik und Quantoren}
Unser ''Einsetzprinzip'' mit Variablen soll im Folgenden in einem genaueren Rahmen erklärt werden. Wir gelangen auf diesem Wege zur Prädikatenlogik, welche diesem Prinzip entspricht und zusammen mit der Aussagenlogik eine Beschreibemöglichkeit mathematischer Zusammenhhänge liefert.

% ABSCHNITT 2.9
\section{Definition}
Ein \emph{Prädikat} ist ein Ausdruck mit Platzhaltern (\emph{Variablen} so, dass wann immer man die Variablen durch Objekte des für die Variablen erlaubten Geltungsbereichs ersetzt (d.h. etwas EINSETZT) eine Aussage entsteht.

% ABSCHNITT 2.10
\section{Beispiel}

\begin{itemize}
    \setlength\itemsep{0em}
    \item ''Jede gerade Quadratzahl ist durch 4 teilbar'' ist eine \emph{atomare} Aussage ohne Variable. Gemeint ist: 4 ist durch 4 teilbar, 16 ist durch 4 teilbar, 36 ist durch 4 teilbar...
    \bracetext{Beispiele für Prädikate}{$\rightarrow$ Wollen sagen: ''Ist x eine gerade Quadratzahl, dann ist x durch 4 teilbar.''\\
    (x = Variable, wo gerade Quadratzahlen eingesetzt werden dürfen)

    \item ''x-y ist eine Quadratzahl'' (x,y sind Zahlen, etwa natürliche Zahlen)}

     $\rightarrow$ Bsp.: 3-5 (x=3, y=5) ist eine Quadratzahl (f), 29-4 (x=29, 4=y) ist eine Quadratzahl (w).
    \item ''$x>2\Rightarrow x^2>4$'', welche wahr für alle reellen Zahlen x ist

            Bsp. $3>2\Rightarrow 3^{2}>4$, da 3>2 \emph{w} und $3^2>4$ \emph{w}, etc.,

            vgl. ''ex falso quodlibet''
    \item ''Ist x gerade Zahl, mindestens 4, dann ist x Summe von zwei Primzahlen'', (Goldbachsche Vermutung), Wahrheitswert unbekannt.\\

\end{itemize}
Durch Hinzufügen von \emph{Quantoren} an Prädikatenlogik entstehen neue Aussagen:

% ABSCHNITT 2.11
\section{Definition:}

Sei ein Prädikat P(x) gegeben, wo x die Variable eines bestimmten Geltungsbereichs ist.
\begin{itemize}
    \setlength\itemsep{0em}
    \item Der \emph{Allquantor} $\forall$ bedeutet ''für alle'',

        in Formeln: $\forall x:P(x)$ ist die Aussage ''Für alle x ist P(x) richtig'',

        also entsteht durch Einsetzen eines beliebigen x des Geltungsbereichs die wahre Aussage P(x).
        \item Der \emph{Existenzquantor} $\exists$ bedeutet ''Es existiert'',

            in Formeln: $\exists x:P(x)$ ist die Aussage

            \quad ''Es gibt (mindestens) ein x so, dass P(x) richtig ist'',

            also entsteht durch Einsetzen von mindestens einem x des Geltungsbereichs die wahre Aussage P(x).

            Den Doppelpunkt spricht man meist als ''so, dass''.
\end{itemize}

% ABSCHNITT 2.12
\section{Bemerkung}
\begin{itemize}
    \setlength\itemsep{0em}
    \item[1.] Ist P(x) ein Prädikat von nur einer Variablen x, so ''bindet'' ein Quantor diese Variable und es entsteht eine Aussage.
    \item[2.] Ist P(x,y,z...) ein Prädikat von mehreren Variablen x,y,z,..., so entsteht durch Quantifizierung in x ein Prädikat in den Variablen y,z...,

    (nämlich $\forall x:P(x,y,z,...),\quad \exists x:P(x,y,z,...),\quad \exists ! x:P(x,y,z,...)$).
    \item[3.] ''Es gibt höchstens ein x...'' Kann zurückgeführt werden auf\\ ''Es gibt kein x mit..., oder es gibt genau ein x mit...''

    (''Es gibt kein x mit...'' ist die Negation von "es gibt ein x mit...'')

    Dafür wird (meist) kein separates Quantorenzeichen benutzt
\end{itemize}

% ABSCHNITT 2.13
\section{Regeln zur Negation von Prädikaten mit Quantoren:}

\begin{center}
Es gilt: Bei Negation ``kehren sich die Quantoren um''.

\begin{align*}
\lnot(\forall x:P(x))&\Leftrightarrow\exists x:\lnot P(x)\\
\lnot(\exists x:P(x))&\Leftrightarrow\forall x:\lnot P(x)\\
\end{align*}
\end{center}

% ABSCHNITT 2.14
\section{Die Reihenfolge von Quantoren ist wesentlich:}

Sei t eine Variable für Töpfe, d eine Variable für Deckel. Weiter sei P(d,t) das Prädkat ''Der Deckel d passt auf den Topf t''. Dann ist:

$\forall t\ \exists d:P(t,d)$ bedeutend zu ''Zu jedem Topf gibt es einen passenden Deckel.''

$\exists d\ \forall t:P(t,d)$ bedeutend zu ''Es gibt einen Deckel, der passt auf jeden Topf.''\\
Das bedeutet offensichtlich etwas verschiedenes!

% ABSCHNITT 2.15
\section{Verwendung von Quantoren:}

\begin{tabular}{l c p{16em}}
  & $\forall t, G(t): H(t)$ & ``Alle Tische, die grün sind, sind 1m hoch''\\
  ist äqu. zu & $\forall t : G(t) \Rightarrow H(t)$ & ``Alle Tische erfüllen: ist der Tisch grün, dann ist er 1m hoch''\\
  ist \emph{nicht} äqu. zu & $\forall t: G(t) \land H(t)$ & ``Alle Tische sind grün und 1m hoch''\\
  & $\exists t, G(t) : H(t)$ & ``Es gibt einen grünen Tisch, der 1m hoch ist''\\
  ist äqu. zu & $\exists t : G(t) \land H(t)$ & ``Es gibt einen Tisch, der grün ist und 1m hoch''\\
  ist \emph{nicht} äqu. zu & $\exists t : G(t) \Rightarrow H(t)$ & ``Es gibt einen Tisch mit der Eigenschaft: Wenn er grün ist, dann ist er 1m hoch''
\end{tabular}

% $\mbox[1]\left\{\begin{array}{} {\makebox[5.5cm]{{\makebox[2.5cm]{\hfill}}\forall t, G(t):H(t)\hfill}}\mbox{''Alle Tische, die grün sind, sind 1m hoch''} \\
% {\makebox[5.5cm]{{\makebox[2.5cm]{ist äqu. zu:\hfill}}\forall t:G(t)\Rightarrow H(t)\hfill}}\mbox{''Alle Tische erfüllen: ist der Tisch grün,}\\
% {\makebox[5.5cm]{{\makebox[2.5cm]{\hfill}}\hfill}}\mbox{ dann ist er 1m hoch''}
% \end{array}\right.$\\

%  $\makebox[0.75cm]{} {\makebox[5.5cm]{{\makebox[2.5cm]{\textbf{!}\emph{nicht} äqu. zu:\hfill}}\forall t:G(t)\land H(t)\hfill}}\mbox{''Alle Tische sind grün und 1m hoch''}$\\

%  $\mbox[2]\left\{\begin{array}{} {\makebox[5.5cm]{{\makebox[2.5cm]{\hfill}}\exists t, G(t):H(t)\hfill}}\mbox{''Es gibt einen grünen Tisch, der 1m hoch ist''}\\ {\makebox[5.5cm]{{\makebox[2.5cm]{ist äqu. zu:\hfill}}\exists t:G(t)\land H(t)\hfill}}\mbox{''Es gibt einen Tisch, der grün ist und 1m hoch''}\end{array}\right.$\\

%  $\makebox[0.75cm]{} {\makebox[5.5cm]{{\makebox[2.5cm]{\textbf{!}\emph{nicht}
% äqu. zu:\hfill}}\exists t:G(t)\Rightarrow H(t)\hfill}}\mbox{''Es gibt einen Tisch mit der Eigenschaft:}$\\
% $\makebox[0.75cm]{} {\makebox[5.5cm]{{\makebox[2.5cm]{\hfill}}\hfill}}\mbox{Wenn er grün ist, dann ist er 1m hoch''}$\\
% $\makebox[0.75cm]{} {\makebox[5.5cm]{{\makebox[2.5cm]{\hfill}}\hfill}}\mbox{(ist auch wahr, wenn es hier einen roten Tisch gibt!)}$\\
